const pool = require('../utils/postgres-conn');

const driversDal = {
    async getDriversBySeason(season) {
        const query = `SELECT DISTINCT
            d."driverId",
            d.forename,
            d.surname,
            d.nationality,
            d.favorite,
            ds.position,
            ds.points,
            ds.wins
        FROM drivers d 
        LEFT JOIN
        "driverStandings" ds 
        on ds."driverId" = d."driverId" 
        LEFT JOIN
        races r 
        on r."raceId" = ds."raceId" 
        WHERE
            ds."raceId" = r."raceId" 
        AND ds."driverId" = d."driverId" 
        AND r.year = '${season}' 
        AND r.round = (
            SELECT
                MAX(round) 
            FROM
                races r 
        LEFT JOIN
         "driverStandings" ds 
         ON ds."raceId" = r."raceId" 
        WHERE r.year = '${season}')
        ORDER BY ds.wins desc, ds.points desc;`
        return new Promise((resolve, reject) => {
            pool.query(query, (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(results.rows);
            })
        });
    },

    async likeDriverById(id, value) {
        const query = `UPDATE drivers
                            SET favorite = '${value}' 
                            WHERE "driverId" = '${id}';`
        return new Promise((resolve, reject) => {
            pool.query(query, (error) => {
                if (error) {
                    reject(error)
                }
                resolve(value);
            })
        });
    }
}

module.exports = driversDal;
