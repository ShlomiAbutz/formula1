const pool = require('../utils/postgres-conn');

const driverDal = {
    async getDriverById(id) {
        const query = `SELECT
  ds."raceId",
  r.year,
  c."name",
  ds."position",
  ds.points,
  COUNT(ps) AS total_pit_stops,
  MAX(ps.time) AS slowest_pit_stop,
  MIN(ps.time) AS fastest_pit_stop,
  AVG(CAST(lt.milliseconds AS int)) AS avg_lap_time,
  MAX(CAST(lt.milliseconds AS int)) AS max_lap_time,
  MIN(CAST(lt.milliseconds AS int)) AS min_lap_time
FROM "driverStandings" ds
LEFT JOIN races r
  ON r."raceId" = ds."raceId"
LEFT JOIN circuits c
  ON c."circuitId" = r."circuitId"
LEFT JOIN (SELECT
  "raceId",
  time,
  "driverId"
FROM "pitStops" ps
GROUP BY "raceId",
         "driverId",
         time) ps
  ON ps."raceId" = ds."raceId"
  AND ps."driverId" = ds."driverId"
LEFT JOIN (SELECT
  "raceId",
  milliseconds,
  "driverId"
FROM "lapTimes" 
GROUP BY "raceId",
         "driverId",
         milliseconds) lt
  ON lt."raceId" = ds."raceId"
  AND lt."driverId" = ds."driverId"
WHERE ds."driverId" = '${id}'
GROUP BY ds."raceId",
         r.year,
         c.name,
         ds.position,
         ds.points
ORDER BY r."year" DESC, ds."raceId" DESC;`
        return new Promise((resolve, reject) => {
            pool.query(query, (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(results.rows);
            })
        });
    }
}

module.exports = driverDal;
