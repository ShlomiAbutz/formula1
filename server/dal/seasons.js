const pool = require('../utils/postgres-conn');

const seasonsDal = {
    async getAllSeasons() {
        const query = 'select * from seasons s order by year desc;';
        return new Promise((resolve, reject) => {
            pool.query(query, (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(results.rows);
            })
        });
    }
}

module.exports = seasonsDal;
