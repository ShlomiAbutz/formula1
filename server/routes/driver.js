const express = require('express');
const router = express.Router();
const dal = require('../dal/driver')

/* GET a specific driver with all of his races from all the years he is in Formula 1 sorted by the date. */
router.get('/:id', async (req, res, next) => {
    const {id} = req.params;
    try {
        const result = await dal.getDriverById(id)
        res.status(200).json(result)
    } catch (err) {
        throw err
    }
});

module.exports = router;
