const express = require('express');
const router = express.Router();
const dal = require('../dal/drivers')

/* GET drivers list by year. */
router.get('/:season', async (req, res, next) => {
    const {season} = req.params;
    const result = await dal.getDriversBySeason(season)
    res.status(200).json(result)
});

/* POST like driver. */
router.post('/like', async (req, res, next) => {
    const {id, value} = req.body;
    const result = await dal.likeDriverById(id, value);
    res.status(200).json(result)
});

module.exports = router;
