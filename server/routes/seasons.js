const express = require('express');
const router = express.Router();
const dal = require('../dal/seasons')

/* GET seasons list. */
router.get('/', async (req, res, next) => {
    const result = await dal.getAllSeasons()
    res.status(200).json(result)
});

module.exports = router;
