const Pool = require('pg').Pool;
const config = require('../config/dev.json').postgres
const pool = new Pool(config)

module.exports = pool;