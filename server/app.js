const createError = require('http-errors');
const cors = require('cors');
const express = require('express');
const app = express();

const driversRouter = require('./routes/drivers');
const driverRouter = require('./routes/driver');
const seasonsRouter = require('./routes/seasons');

app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/drivers', driversRouter);
app.use('/driver', driverRouter);
app.use('/seasons', seasonsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});

module.exports = app;
