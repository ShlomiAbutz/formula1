# Formula 1 app

## Description
This app can show Formula1 drivers statistics using ergast API

## Requirements

### For development, you will need 
     Node.js v12.0.0
     npm 6.9.0
     psql (PostgreSQL) 12.4 
     Angular 10.1.2
   installed in your environment.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm
      
---
      
### Install the Angular CLI
      $ npm install -g @angular/cli
      
### Install PostgreSQL
      $ sudo apt update
      $ sudo apt install postgresql postgresql-contrib
      
---
      
### Install Packages      
     $ cd formula1/app
     $ npm install
     
     $ cd formula1/server
     $ npm install

### Populate database
     $ cd formula1/server/db
     $ bash init.sh
      
### Running the project
    $ cd formula1/app
    $ npm start
        
    $ cd formula1/server
    $ node server.js
      
