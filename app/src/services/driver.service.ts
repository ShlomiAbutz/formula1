import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  private TAG: string = `[${this.constructor.name}]`;
  constructor(private http: HttpClient) {}

  public getDriverById(driverId) {
    return this.http.get(`http://127.0.0.1:3000/driver/${driverId}`);
  }
}
