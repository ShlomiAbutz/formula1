import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {Season} from "../models/season.interface";
import {DriverData} from "../models/driver-data.interface";

@Injectable({
  providedIn: 'root'
})
export class DriversService {
  private TAG: string = `[${this.constructor.name}]`;
  constructor(private http: HttpClient) {}

  /** GET: all drivers by year from the database */
  public getDriversBySeason(seasonYear: string):Observable<DriverData[]> {
    return this.http.get<DriverData[]>(`http://127.0.0.1:3000/drivers/${seasonYear}`);
  }

  /** POST: like a driver */
  public likeDriverById(driverId: string, isLiked: boolean): Observable<boolean> {
    console.log(this.TAG, driverId);
    return this.http.post<boolean>('http://127.0.0.1:3000/drivers/like', {id: driverId, value: isLiked});
  }
}
