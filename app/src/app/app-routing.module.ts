import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DriversComponent} from "../components/drivers/drivers.component";
import {DriverComponent} from "../components/driver/driver.component";

const routes: Routes = [
  { path: '', component: DriversComponent },
  { path: 'drivers', component: DriversComponent },
  { path: 'driver/:id', component: DriverComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
