import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Race} from "../../models/race.interface";
import {ObservableSubscriptionComponent} from "../../utils/ observable-subscription-component.util";
import {DriverService} from "../../services/driver.service";
import {ActivatedRoute} from "@angular/router";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";


@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class DriverComponent extends ObservableSubscriptionComponent implements OnInit {
  driverId: string;
  races: Race[];
  displayedColumns: string[] = ['name', 'avg_lap_time', 'max_lap_time', 'min_lap_time', 'points', 'position',
    'slowest_pit_stop', 'total_pit_stops'];
  dataSource: MatTableDataSource<Race>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private driverService: DriverService, private route: ActivatedRoute) {
    super()
  }

  ngOnInit(): void {
    this.driverId = this.route.snapshot.paramMap.get('id')
    super.ngOnInit()
    this.getDriverById(this.driverId);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy()
  }

  private initRaces(races: any[]) {
    this.races = races;
    this.dataSource = new MatTableDataSource(this.races);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getDriverById(driverId) {
    this.driverService.getDriverById(driverId)
      .subscribe((data: any) => {
        this.initRaces(data);
      });
  }

  public millisToMinutes(millis) {
    const minutes = Math.floor(millis / 60000);
    const seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (Number(seconds) < 10 ? '0' : '') + seconds;
  }
}
