import {Component, OnInit, ViewChild} from '@angular/core';
import {DriversService} from "../../services/drivers.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {SeasonsService} from "../../services/seasons.service";
import {DriverData} from "../../models/driver-data.interface";
import {ObservableSubscriptionComponent} from "../../utils/ observable-subscription-component.util";
import {Season} from "../../models/season.interface";

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent extends ObservableSubscriptionComponent implements OnInit {
  seasons: Season[];
  drivers: DriverData[];
  displayedColumns: string[] = ['avatar', 'name', 'nationality', 'points', 'position', 'wins', 'like'];
  dataSource: MatTableDataSource<DriverData>;
  selectedSeason: Season;
  selectedDriver: DriverData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private seasonsService: SeasonsService,
              private driversService: DriversService) {
    super()
  }

  ngOnInit(): void {
    super.ngOnInit()
    this.getAllSeasons();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy()
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private initDrivers(data: DriverData[]): void {
    this.drivers = data;
    this.dataSource = new MatTableDataSource(this.drivers);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getAllSeasons() {
    this.seasonsService.getAllSeasonsList()
      .subscribe((data: any) => {
        this.initSeasons(data);
        this.getAllDrivers(data[1]);
      });
  }

  private initSeasons(data: Season[]): void {
    this.seasons = data;
    this.selectedSeason = this.seasons[1];
  }

  private getAllDrivers(season: Season): void {
    this.driversService.getDriversBySeason(season.year)
      .subscribe((data: DriverData[]) => {
        this.initDrivers(data);
      });
  }

  public seasonChangeHandler(season: Season): void {
    this.getAllDrivers(season);
  }

  public likeHandler(event, driverId: string, isLiked: boolean): void {
    event.stopPropagation();
    this.driversService.likeDriverById(driverId, isLiked)
      .subscribe((result: boolean) => {
        const driversArray: DriverData[] = [...this.drivers]
        const driverIndex = driversArray.findIndex(driver => driver.driverId === driverId)
        driversArray[driverIndex].favorite = isLiked;
        this.initDrivers(driversArray);
      })
  }
}
