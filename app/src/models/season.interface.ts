export interface Season {
  year: string;
  url: string;
}
