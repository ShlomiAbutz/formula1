export interface Race {
  avg_lap_time: string;
  fastest_pit_stop: string;
  max_lap_time: number;
  min_lap_time: number;
  name: string;
  points: string;
  position: string;
  raceid: string;
  slowest_pit_stop: string;
  total_pit_stops: string;
  year: string;
}
