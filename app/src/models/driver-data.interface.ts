export interface DriverData {
  driverId: string;
  forename: string;
  surname: string;
  nationality: string;
  points: string;
  position: string;
  wins: string;
  favorite: boolean;
}
